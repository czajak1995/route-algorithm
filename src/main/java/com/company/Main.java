package com.company;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        FileWriter fileWriter = null;

        String pattern = "yyyy-MM-dd_HH-mm-ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());
        String type = "random";

        ConstraintsContext<Double> constraintsContext = new RouteConstraintsContext();
        constraintsContext.addConstraint(ConstraintType.MAX_DISTANCE, 13.51); // 17.51
        constraintsContext.addConstraint(ConstraintType.MAX_DURATION, 0.76); // 0.76

        Point startPoint = new Point(7, 8);
        Point endPoint = new Point(7, 0);
        Area area = new CircleArea(startPoint, endPoint);
        Set<RoutePoint> points = area.filter(TestSet.getOrders());


        try {
            fileWriter = new FileWriter("C:\\Users\\vxjf37\\Documents\\priv\\master-thesis\\route-algorithm\\src\\main\\java\\com\\company\\logs\\logs-" + type + "-" + date + ".log");

            fileWriter.write("Algorithm test - " + date + "\n");
            writeProperties(fileWriter);

            fileWriter.write("\n");
            fileWriter.write("Constraints\n");
            fileWriter.write("max distance: " + constraintsContext.getConstraints().get(ConstraintType.MAX_DISTANCE) +" [km]\n");
            fileWriter.write("max duration: " + constraintsContext.getConstraints().get(ConstraintType.MAX_DURATION) + " [h]\n");

            fileWriter.write("\n\n");
            fileWriter.write("Results:\n");
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<Double> incomes = new ArrayList<>();

        for(int i = 0; i < 10; i++) {

            try {
                long lStartTime = new Date().getTime();

                RouteManager routeManager = new RouteManager(points, startPoint, endPoint, constraintsContext);

                Route route = routeManager.getBestRoute();

                long lEndTime = new Date().getTime();

                double lDuration = (double) (lEndTime - lStartTime) / (1000.);
                if(route != null) {
                    fileWriter.write(route + "\n");
                    fileWriter.write("sections count: " + route.getSections().size() + "\n");
                    fileWriter.write("income: " + route.getTotalIncome() + " [PLN]\n");
                    fileWriter.write("loss: " + route.getTotalLoss() + " [PLN]\n");
                    fileWriter.write("distance: " + route.getTotalDistance() + " [km]\n");
                    fileWriter.write("duration: " + route.getTotalDuration() + " [h]\n");
                    fileWriter.write("algorithm time taken: " + lDuration + " [s]\n");
                    fileWriter.write("##### real income: " + (route.getTotalIncome() - route.getTotalLoss()) + " [PLN] #####\n");
                    fileWriter.write("\n");

                    incomes.add(route.getTotalIncome() - route.getTotalLoss());
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            fileWriter.write("\n\n");
            fileWriter.write("Max income: " + Collections.max(incomes) + " [PLN]\n");
            fileWriter.write("Min income: " + Collections.min(incomes) + " [PLN]\n");
            fileWriter.write("Average income: " + incomes.stream().mapToDouble(v -> v).average().orElse(0.0) + " [PLN]\n");

            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void writeProperties(FileWriter writer) throws IOException {
        Map<String, Number> algorithmProperties = new LinkedHashMap<String, Number>() {{
            put("iteration", AlgorithmProperties.iterations);
            put("initial sections count", AlgorithmProperties.sectionsCount);
            put("routes count", AlgorithmProperties.routesCount);
            put("best count", AlgorithmProperties.bestRoutesCount);
            put("good count", AlgorithmProperties.goodRoutesCount);
            put("poor count", AlgorithmProperties.poorRoutesCount);
            put("best neighbourhood", AlgorithmProperties.bestRoutesNeighbourhoodSize);
            put("good neighbourhood", AlgorithmProperties.goodRoutesNeighbourhoodSize);
            put("increase threshold", IterationController.increasePopulationThreshold);
            put("decrease threshold", IterationController.decreasePopulationThreshold);
            put("time over margin", IterationController.timeOverMarginThreshold);
            put("time under margin", IterationController.timeUnderMarginThreshold);
            put("distance over margin", IterationController.distanceOverMarginThreshold);
            put("distance under margin", IterationController.distanceUnderMarginThreshold);
        }};

        writer.write("Properties:\n");

        for(Map.Entry<String, Number> entry : algorithmProperties.entrySet()) {
            writer.write(entry.getKey() + ": " + entry.getValue());
            writer.write("\n");
        }

        writer.write("\n");
    }
}
