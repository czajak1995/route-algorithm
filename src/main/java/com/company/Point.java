package com.company;

import java.util.Objects;

public class Point implements Cloneable {

    private double x;
    private double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    @Override
    public String toString() {
        return "(" + x +
                ", " + y +
                ')';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return Double.compare(point.x, x) == 0 &&
                Double.compare(point.y, y) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    public Double calculateDistance(Point ref) {
        if(ref != this) return Math.sqrt(Math.pow(this.getX() - ref.getX(), 2) + Math.pow(this.getY() - ref.getY(), 2));
        return Double.MAX_VALUE;
    }
}
