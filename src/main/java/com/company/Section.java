package com.company;

import java.util.Objects;

public class Section {

    private RoutePoint a;
    private RoutePoint b;

    public Section(Section section) {
        this.a = new RoutePoint(section.a, 0);
        this.b = new RoutePoint(section.b, section.getIncome());
    }

    public Section(RoutePoint a, RoutePoint b) {
        this.a = a;
        this.b = b;
    }

    public RoutePoint getA() {
        return a;
    }

    public RoutePoint getB() {
        return b;
    }

    public void setA(RoutePoint a) {
        this.a = a;
    }

    public void setB(RoutePoint b) {
        this.b = b;
    }

    public void update(RoutePoint a, RoutePoint b) {
        this.a = a;
        this.b = b;
    }

    public double getIncome() {
        return b.getIncome();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Section section = (Section) o;
        return Objects.equals(a, section.a) &&
                Objects.equals(b, section.b);
    }

    @Override
    public int hashCode() {
        return Objects.hash(a, b);
    }
}
