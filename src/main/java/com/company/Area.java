package com.company;

import java.util.Set;

public interface Area {
    boolean contains(Point point);
    Set<RoutePoint> filter(Set<RoutePoint> points);
}
