package com.company;

public class RoutePoint extends Point {

    private double income;

    public RoutePoint(Point point, double income) {
        super(point.getX(), point.getY());
        this.income = income;
    }


    public static RoutePoint valueOf(Point point) {
        return new RoutePoint(point, 0);
    }


    public double getIncome() {
        return income;
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
