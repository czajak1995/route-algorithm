package com.company;

public interface SectionCallback {
    void onSectionIncrement();
    void onSectionDecrement();
}
