package com.company;

import java.util.HashMap;
import java.util.List;

public class IterationController {

    HashMap<ConstraintType, Double> constraints;
    private SectionCallback callback;
    private int iterationPeriod;
    private double initialIterationPeriod = 0.05;
    private int lastHandleIteration = 0;
    private int currentIteration = 0;

    public final static double increasePopulationThreshold = 0.5;
    public final static double decreasePopulationThreshold = 0.7;
    public final static double timeOverMarginThreshold = 0.05;
    public final static double distanceOverMarginThreshold = 0.05;
    public final static double timeUnderMarginThreshold = -0.05;
    public final static double distanceUnderMarginThreshold = -0.05;

    private ConstraintsCallback timeCallback = new ConstraintsCallback() {
        @Override
        public double getValue(Route route) {
            return (constraints.get(ConstraintType.MAX_DURATION) - route.getTotalDuration()) / constraints.get(ConstraintType.MAX_DURATION);
        }
    };

    private ConstraintsCallback distanceCallback = new ConstraintsCallback() {
        @Override
        public double getValue(Route route) {
            return (constraints.get(ConstraintType.MAX_DISTANCE) - route.getTotalDistance()) / constraints.get(ConstraintType.MAX_DISTANCE);
        }
    };

    public IterationController(int totalIterationCount, SectionCallback callback, ConstraintsContext<Double> constraintsContext) {
        this.callback = callback;
        this.iterationPeriod = (int) (totalIterationCount * initialIterationPeriod);
        this.constraints = constraintsContext.getConstraints();
    }

    public void update(List<Route> routes) {
        currentIteration++;

        if(shouldBeUpdated()) {

            double populationOverMarginDistanceFactor = calculateOverMarginDistanceFactor(routes);
            double populationOverMarginDurationFactor = calculateOverMarginTimeFactor(routes);
            double populationUnderMarginDistanceFactor = calculateUnderMarginDistanceFactor(routes);
            double populationUnderMarginDurationFactor = calculateUnderMarginTimeFactor(routes);

            if(populationOverMarginDistanceFactor >= increasePopulationThreshold || populationOverMarginDurationFactor >= increasePopulationThreshold) {
                callback.onSectionIncrement();
            } else if(populationUnderMarginDistanceFactor >= decreasePopulationThreshold || populationUnderMarginDurationFactor >= decreasePopulationThreshold) {
                callback.onSectionDecrement();
            }

            lastHandleIteration = currentIteration;
        }
    }

    private boolean shouldBeUpdated() {
        return currentIteration >= lastHandleIteration + iterationPeriod;
    }

    private double calculateOverMarginTimeFactor(List<Route> routes) {
        return calculateOverMarginFactor(routes, timeOverMarginThreshold, timeCallback);
    }

    private double calculateOverMarginDistanceFactor(List<Route> routes) {
        return calculateOverMarginFactor(routes, distanceOverMarginThreshold, distanceCallback);
    }

    private double calculateOverMarginFactor(List<Route> routes, Double threshold, ConstraintsCallback callback) {
        int overMarginRoutesCount = 0;

        for(Route route : routes) {
            if(callback.getValue(route) >= threshold) overMarginRoutesCount++;
        }

        return Double.valueOf(overMarginRoutesCount) / Double.valueOf(routes.size());
    }

    private double calculateUnderMarginTimeFactor(List<Route> routes) {
        return calculateUnderMarginFactor(routes, timeUnderMarginThreshold, timeCallback);
    }

    private double calculateUnderMarginDistanceFactor(List<Route> routes) {
        return calculateUnderMarginFactor(routes, distanceUnderMarginThreshold, distanceCallback);
    }

    private double calculateUnderMarginFactor(List<Route> routes, Double threshold, ConstraintsCallback callback) {
        int underMarginRoutesCount = 0;

        for(Route route : routes) {
            if(callback.getValue(route) <= threshold) underMarginRoutesCount++;
        }

        return Double.valueOf(underMarginRoutesCount) / Double.valueOf(routes.size());
    }

    private interface ConstraintsCallback {
        double getValue(Route route);
    }
}
