package com.company;

import java.util.Map;

public class RouteConstraintsContext extends ConstraintsContext<Double> {




    public static boolean isSatisfied(Route route, ConstraintsContext<Double> constraintsContext) {
        for(Map.Entry<ConstraintType, Double> constraint : constraintsContext.getConstraints().entrySet()) {
            switch(constraint.getKey()) {
                case MAX_DISTANCE:
                    if(route.getTotalDistance() > constraint.getValue()) return false;
                    break;

                case MAX_DURATION:
                    if(route.getTotalDuration() > constraint.getValue()) return false;
                    break;
            }
        }

        return true;
    }
}
