package com.company;

import java.util.HashSet;
import java.util.Set;

public class CircleArea implements Area {

    private double overRadiusFactor = 1.2;
    private Point centrePoint;
    private double baseRadius;

    public CircleArea(Point startPoint, Point endPoint) {
        double distance = Math.sqrt(Math.pow(endPoint.getX() - startPoint.getX(), 2)
                + Math.pow(endPoint.getY() - startPoint.getY(), 2));

        baseRadius = distance / 2;
        centrePoint = new Point(
                (startPoint.getX() + endPoint.getX()) / 2,
                (startPoint.getY() + endPoint.getY()) / 2
        );
    }

    public boolean contains(Point point) {
        double radius = baseRadius * overRadiusFactor;
        double distance = Math.sqrt(Math.pow(point.getX() - centrePoint.getX(), 2) + Math.pow(point.getY() - centrePoint.getY(), 2));
        return distance <= radius;
    }

    public Set<RoutePoint> filter(Set<RoutePoint> points) {
        Set<RoutePoint> filteredPoints = new HashSet<>();

        for(RoutePoint p : points) {
            if(contains(p)) filteredPoints.add(p);
        }

        return filteredPoints;
    }

    public void setOverRadiusFactor(double overRadiusFactor) {
        if (overRadiusFactor > 1) this.overRadiusFactor = overRadiusFactor;
        else throw new IllegalArgumentException("Over radius factor can't be less than 1");
    }

    public double getOverRadiusFactor() {
        return overRadiusFactor;
    }

    public double getRadius() {
        return baseRadius * overRadiusFactor;
    }

    public Point getCentrePoint() {
        return centrePoint;
    }
}
