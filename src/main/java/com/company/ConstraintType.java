package com.company;

public enum ConstraintType {
    MAX_DISTANCE,
    MAX_DURATION
}
