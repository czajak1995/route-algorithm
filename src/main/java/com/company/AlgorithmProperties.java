package com.company;

public class AlgorithmProperties {

    public static int iterations = 50;
    public static int sectionsCount = 4;

    // bees
    public static int routesCount = 30;
    public static int bestRoutesCount = (int) (0.15 * routesCount); // 15%
    public static int goodRoutesCount = (int) (0.2 * routesCount); // 20%
    public static int poorRoutesCount = routesCount - bestRoutesCount - goodRoutesCount; // 65%

    public static int bestRoutesNeighbourhoodSize = 100; // 100
    public static int goodRoutesNeighbourhoodSize = 50; // 50

    // route factors
    public static final double averageVelocity = 40.;
    public static final double averageTimeForStop = 5./60.;
    public static final double distanceCorrectionFactor = 1.3;

    // income factors
    public static final double minIncome = 20.8;
    public static final double fuelLossFactorPerKmInPln = 0.4;
    public static final double profitabilityRatio = minIncome;
}
