package com.company;

import java.util.HashMap;

public class ConstraintsContext<T> {

    private HashMap<ConstraintType, T> constraints = new HashMap<>();

    public void addConstraint(ConstraintType type, T value) {
        constraints.put(type, value);
    }

    protected HashMap<ConstraintType, T> getConstraints() {
        return constraints;
    }
}
