package com.company;

import java.util.*;

public class RouteManager {

    private Set<RoutePoint> points;

    private Point source;
    private Point destination;
    private ConstraintsContext<Double> constraintsContext;

    List<Route> bestRoutes = new ArrayList<>();

    public RouteManager(final Set<RoutePoint> points, Point source, Point destination, final ConstraintsContext<Double> constraintsContext) {
        this.source = source;
        this.destination = destination;
        this.constraintsContext = constraintsContext;
        this.points = points;
    }

    public Route getBestRoute() {
        IterationController iterationController = new IterationController(AlgorithmProperties.iterations, getDefaultSectionCallback(), constraintsContext);

        // 1. generate random bees
        List<Route> savedRoutes = generateOrderedRandomRoutes(AlgorithmProperties.routesCount);
        updateAlgorithmParameters(savedRoutes);

        // 2. Count income for all bees and get the best route in iteration
        sortRoutes(savedRoutes);
        saveBestRouteInIteration(savedRoutes);

        // 3. Iterate 'iterations' times and update best routes
        for (int idx = 1; idx < AlgorithmProperties.iterations; idx++) {
            List<Route> iterationRoutes = new ArrayList<>();
            List<Route> prevBestRoutes = savedRoutes.subList(0, AlgorithmProperties.bestRoutesCount);
            List<Route> prevGoodRoutes = savedRoutes.subList(AlgorithmProperties.bestRoutesCount, AlgorithmProperties.bestRoutesCount + AlgorithmProperties.goodRoutesCount);

            manageRoutes(prevBestRoutes, iterationRoutes, AlgorithmProperties.bestRoutesNeighbourhoodSize);
            manageRoutes(prevGoodRoutes, iterationRoutes, AlgorithmProperties.goodRoutesNeighbourhoodSize);
            managePoorRoutes(iterationRoutes, AlgorithmProperties.poorRoutesCount);

            orderRoutes(iterationRoutes);

            sortRoutes(iterationRoutes);

            saveBestRouteInIteration(iterationRoutes);
            savedRoutes = iterationRoutes;

            iterationController.update(iterationRoutes);
        }

/*        for (Route route : bestRoutes) {
            System.out.println(route);
        }*/

        return getTopRoute(bestRoutes);
    }

    private void manageRoutes(final List<Route> savedRoutes, List<Route> routes, int neighbourhoodSize) {
        for (final Route route : savedRoutes) {
            List<Route> neighbourRoutes = route.getNeighbourRoutes(neighbourhoodSize, points);
            sortRoutes(neighbourRoutes);

            routes.add(neighbourRoutes.get(0));
        }
    }

    private void managePoorRoutes(List<Route> iterationRoutes, int neighbourhoodSize) {
        iterationRoutes.addAll(generateOrderedRandomRoutes(neighbourhoodSize));
    }

    private Route getTopRoute(List<Route> routes) {
        if (routes == null || routes.size() == 0 ||
                !RouteConstraintsContext.isSatisfied(routes.get(routes.size() - 1), constraintsContext)) {
            return null;
        }

        return routes.get(routes.size() - 1);
    }

    private void sortRoutes(List<Route> routes) {
        try {
            Collections.sort(routes, (first, second) -> {
                if (!RouteConstraintsContext.isSatisfied(first, constraintsContext)) {
                    return -1;
                }

                double diff = getOutcomeForRoute(second) - getOutcomeForRoute(first);
                return (diff == 0) ? 0 : diff > 0 ? 1 : -1;
            });
        } catch (IllegalArgumentException iae) {

        }
    }

    private void saveBestRouteInIteration(final List<Route> routes) {
        if (routes == null || routes.size() == 0) return;

        Route route = new Route(routes.get(0));

        if (bestRoutes.isEmpty()) {
            bestRoutes.add(route);
            return;
        }

        final Route lastBestRoute = bestRoutes.get(bestRoutes.size() - 1);

        if (bestRoutes.isEmpty() || getOutcomeForRoute(route) > getOutcomeForRoute(lastBestRoute)) {
            bestRoutes.add(route);
        } else {
            bestRoutes.add(lastBestRoute);
        }
    }

    private double getOutcomeForRoute(final Route route) {
        if (!RouteConstraintsContext.isSatisfied(route, constraintsContext)) {
            return (-1.) * route.getTotalLoss();
        }

        double income = route.getTotalIncome();
        double loss = route.getTotalLoss();

        return income - loss;
    }

    private List<Route> generateOrderedRandomRoutes(int amount) {
        List<Route> routes = generateRandomRoutes(amount);
        orderRoutes(routes);
        return routes;
    }

    private List<Route> generateRandomRoutes(int amount) {
        Set<Route> routes = new HashSet<>();

        int maxTrialCount = 100;
        int idx = 0;

        while (routes.size() < amount && idx++ < maxTrialCount) {
            Route route = Route.generate(source, destination, points, AlgorithmProperties.sectionsCount);
            if (route != null) routes.add(route);
        }

        return new ArrayList<>(routes);
    }

    private void orderRoutes(List<Route> routes) {
        for (Route route : routes) {
            route.order();
        }
    }

    private void updateAlgorithmParameters(List<Route> routes) {
        AlgorithmProperties.routesCount = routes.size();

        AlgorithmProperties.bestRoutesCount = (int) (0.25 * AlgorithmProperties.routesCount);
        AlgorithmProperties.goodRoutesCount = (int) (0.3 * AlgorithmProperties.routesCount);
        AlgorithmProperties.poorRoutesCount = AlgorithmProperties.routesCount - AlgorithmProperties.bestRoutesCount - AlgorithmProperties.goodRoutesCount;
    }

    private SectionCallback getDefaultSectionCallback() {
        SectionCallback callback = new SectionCallback() {
            @Override
            public void onSectionIncrement() {
                AlgorithmProperties.sectionsCount++;
            }

            @Override
            public void onSectionDecrement() {
                if (AlgorithmProperties.sectionsCount > 2) AlgorithmProperties.sectionsCount--;
            }
        };

        return callback;
    }

}
