package com.company;

import java.util.*;

// Bee
public class Route {

    LinkedList<Section> sections;

    private Route() {
        sections = new LinkedList<>();
    }

    private Route(LinkedList<Section> sections) {
        this.sections = sections;
    }

    public Route(Route route) {
        this.sections = new LinkedList<>();
        this.sections.addAll(route.sections);
    }

    public static Route generate(Point source, Point destination, final Set<RoutePoint> allPoints, int sectionsCount) {
        Route route = new Route();
        RoutePoint first;
        RoutePoint second = RoutePoint.valueOf(source);

        int maxTrialCount = 100;
        int currentIdx = 0;

        Set<RoutePoint> points = new HashSet<>();
        points.addAll(allPoints);

        for(int idx = 0; idx < sectionsCount - 1; idx++) {
            if(idx == 0) first = RoutePoint.valueOf(source);
            else first = second;

            do {
                second = choosePointFromSet(points);
                if(second != null) points.remove(second);
            } while(second.equals(first) && currentIdx++ < maxTrialCount);

            if(currentIdx >= maxTrialCount) {
                return null;
            }

            currentIdx = 0;

            route.sections.add(new Section(first, second));
        }

        route.sections.add(new Section(second, RoutePoint.valueOf(destination)));

        return route;
    }

    public void order() {
        RoutePoint startPoint = sections.get(0).getA();
        RoutePoint endPoint = sections.get(sections.size() - 1).getB();
        Set<RoutePoint> points = new HashSet<>();

        for(Section section : sections) {
            points.add(section.getB());
        }

        points.remove(endPoint);

        RoutePoint first = startPoint;
        RoutePoint second;

        Iterator<Section> iterator = sections.iterator();
        Section currentSection = iterator.next();

        while(true) {
            if(!iterator.hasNext()) break;

            second = getNearestPoint(first, points);
            currentSection.update(first, second);

            first = second;
            points.remove(second);

            currentSection = iterator.next();
        }

        currentSection.update(first, endPoint);
    }

    private RoutePoint getNearestPoint(RoutePoint ref, Set<RoutePoint> points) {
        Map<RoutePoint, Double> distancesMap = new HashMap<>();
        for(RoutePoint point : points) {
            distancesMap.put(point, point.calculateDistance(ref));
        }

        Map.Entry<RoutePoint, Double> min = Collections.min(distancesMap.entrySet(), Comparator.comparing(Map.Entry::getValue));

        return min.getKey();
    }

    public double getTotalIncome() {
        double totalIncome = 0;

        for(Section section : sections) {
            totalIncome += section.getIncome();
        }

        return totalIncome;
    }

    public double getTotalLoss() {
        return getTotalDistance() * AlgorithmProperties.fuelLossFactorPerKmInPln + getTotalDuration() * AlgorithmProperties.profitabilityRatio;
    }

    public double getTotalDistance() {
        double totalDistance = 0;

        for(Section section : sections) {
            totalDistance += section.getB().calculateDistance(section.getA());
        }

        return AlgorithmProperties.distanceCorrectionFactor * totalDistance;
    }

    public double getTotalDuration() {
        double totalDistance = getTotalDistance();
        double totalStopTime = (getSections().size() - 1) * AlgorithmProperties.averageTimeForStop;

        double totalDuration = totalDistance / AlgorithmProperties.averageVelocity + totalStopTime;

        return totalDuration;
    }

    public List<Route> getNeighbourRoutes(int neighbourhoodSize, Set<RoutePoint> points) {
        List<Route> routes = new ArrayList<>();
        routes.add(this);

        for(int i = 0; i < neighbourhoodSize; i++) {
            Route modifiedRoute = modifySectionInRoute(sections.size(), points);
            routes.add(modifiedRoute);
        }

        return routes;
    }

    private Route modifySectionInRoute(int sectionsCount, Set<RoutePoint> points) {
        int randomSectionId = new Random().nextInt(sectionsCount - 1);
        LinkedList<Section> routeSections = new LinkedList<>();
        for(Section section : sections) {
            routeSections.add(new Section(section));
        }

        Section endOfSection = routeSections.get(randomSectionId);
        Section startOfSection = routeSections.get(randomSectionId + 1);

        List<RoutePoint> allPoints = new ArrayList<>(points);
        allPoints.removeAll(getRoutePointsInRoute());

        RoutePoint randomPoint = allPoints.get(new Random().nextInt(allPoints.size()));

        endOfSection.setB(randomPoint);
        startOfSection.setA(randomPoint);

        return new Route(routeSections);
    }

    private static RoutePoint choosePointFromSet(Set<RoutePoint> points) {
        int size = points.size();
        int itemIdx = new Random().nextInt(size);

        int currentIdx = 0;
        for(RoutePoint p : points) {
            if(currentIdx++ == itemIdx) return p;
        }

        return null;
    }

    public LinkedList<Section> getSections() {
        return sections;
    }

    public Set<RoutePoint> getRoutePointsInRoute() {
        Set<RoutePoint> routePoints = new HashSet<>();

        for(Section section : sections) {
            routePoints.add(section.getA());
            routePoints.add(section.getB());
        }

        return routePoints;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(sections.get(0).getA() + " -> ");

        for(int i = 0; i < sections.size() - 1; i++) {
            builder.append(sections.get(i).getB() + " -> ");
        }

        builder.append(sections.get(sections.size() - 1).getB());

        return builder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Route route = (Route) o;
        return Objects.equals(sections, route.sections);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sections);
    }
}
