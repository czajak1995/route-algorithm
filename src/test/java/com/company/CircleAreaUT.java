package com.company;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CircleAreaUT {

    private static CircleArea area;
    private static double overRadiusFactor;

    @BeforeClass
    public static void setUp() {
        Point startPoint = new Point(0, 5);
        Point endPoint = new Point(5, 0);

        area = new CircleArea(startPoint, endPoint);

        overRadiusFactor = 1.2;
    }

    @Test
    public void IsPointInsideArea() {
        List<Point> pointsToTest = Arrays.asList(
                new Point(0, 0),
                new Point(1, 2),
                new Point(5, 5)
        );

        for(Point point : pointsToTest) {
            assertTrue("Point " + point + " isn't inside area", area.contains(point));
        }
    }

    @Test
    public void IsNotPointInsideArea() {
        List<Point> pointsToTest = Arrays.asList(
                new Point(10, 10),
                new Point(-3, 3),
                new Point(-3, -3)
        );

        for(Point point : pointsToTest) {
            assertFalse("Point " + point + " is inside area", area.contains(point));
        }
    }

    @Test
    public void IsPointInsideAreaCornerCase() {
        double radius = area.getRadius();
        Point centre = area.getCentrePoint();

        List<Point> cornerPoints = Arrays.asList(
                new Point(centre.getX() + radius, centre.getY()),
                new Point(centre.getX(), centre.getY() - radius),
                new Point(centre.getX() - radius, centre.getY()),
                new Point(centre.getX(), centre.getY() + radius)
        );

        for(Point point : cornerPoints) {
            assertTrue("Point " + point + " isn't inside area", area.contains(point));
        }
    }

    @Test
    public void IsNotPointInsideAreaCornerCase() {
        double radius = area.getRadius();
        Point centre = area.getCentrePoint();
        double delta = 0.001;

        List<Point> cornerPoints = Arrays.asList(
                new Point(centre.getX() + radius + delta, centre.getY()),
                new Point(centre.getX(), centre.getY() - radius - delta),
                new Point(centre.getX() - radius - delta, centre.getY()),
                new Point(centre.getX(), centre.getY() + radius + delta)
        );

        for(Point point : cornerPoints) {
            assertFalse("Point " + point + " is inside area", area.contains(point));
        }
    }

}
